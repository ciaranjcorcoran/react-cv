import React, { Component } from 'react';
import './App.sass';
import data from './Components/jobs.json';
import Footer from './Components/Footer/Footer';
import HeroSection from './Components/Header/HeroSection';
import Main from './Components/main';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      jobList: data,
      filteredJobList: data
    };
  }

  jobSearch(term) {
    let results = this.state.jobList.filter((result) => {
      return result.skills.toLowerCase().indexOf(term.toLowerCase()) !== -1;
    });
    this.setState({filteredJobList: results});
  }

  render() {

    return (
      <div className="App">
        <HeroSection />
        <Main 
          jobList={this.state.filteredJobList}
          searchJobs={this.jobSearch.bind(this)} />
        <Footer></Footer>
      </div>
    );
  }
}

export default App;
