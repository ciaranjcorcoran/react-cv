import React from 'react';

const footer = () => {
  return (
  <footer>
    <div class="container text-align-center">
				<div class="social-links">
					<a href="https://twitter.com/C0rcs" target="_blank" rel="noopener noreferrer"><i className="icon-twitter"></i> Twitter</a>
					<a href="https://www.linkedin.com/in/ciar%C3%A1n-corcoran-b3a59540/" target="_blank" rel="noopener noreferrer"><i className="icon-linkedin"></i>Linkedin</a>
					<a href="https://instagram.com/cjcorcs" rel="noopener noreferrer"><i className="icon-instagram"></i>Instagram</a>
          <a href="https://bitbucket.org/ciaranjcorcoran" rel="noopener noreferrer"><i className="icon-embed"></i>Bitbucket</a>
				</div>
			</div>
    <p class="copyright">© 2019 Ciarán Corcoran | Front end developer in London</p>
  </footer>);
}

export default footer;