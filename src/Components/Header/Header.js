import React from 'react';

const header = (props) => {
  return (
    <header>
      <h1>Ciarán <span>Corcoran</span></h1>
    </header>
  );
}

export default header;