import React from 'react';
// import SearchBar from './SeachBar';
import PropTypes from 'prop-types';

const HeroSection = (props) => {
  return (
    <div className="hero-section">
      <h1>Ciarán Corcoran</h1>
      <h2>Front End Developer</h2>
      <p>I am a highly adaptable and personable Front End Developer with solid experience in HTML5/CSS3, Javascript, various frameworks/libraries and plenty of experience with responsive/mobile development.</p>
      {/* <SearchBar getJobs={props.searchJobs} /> */}
      <p className="prompt">Have a look at my CV below and use the search bar to filter through any technologies I've used.</p>
      <p>This site is built in <strong>React.js</strong> and the source can be viewed on <a href="https://bitbucket.org/ciaranjcorcoran" target="_blank" rel="noopener noreferrer"><strong>Bitbucket</strong></a> along with some other sample projects</p>
      {/* <a href="" className="contact">Get in touch</a> */}
    </div>
  );
}

export default HeroSection;

HeroSection.propTypes = {
    searchJobs: PropTypes.func.isRequired
};