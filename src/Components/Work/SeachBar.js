import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

class SearchBar extends Component {
  state = {
    term: ''
  }

  onInputChange(term) {
    this.setState({term});
    this.props.getJobs(term);
  }

  render() {
    return (
      <Fragment>
        <input type="text"
          placeholder="Filter skills by input..."
          value={this.state.term}
          onChange={event => this.onInputChange(event.target.value)}
        />
      </Fragment>
    );
  }
}

export default SearchBar;

SearchBar.propTypes = {
  searchJobs: PropTypes.func
};