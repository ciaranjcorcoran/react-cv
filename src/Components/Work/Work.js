import React from 'react';
import PropTypes from 'prop-types';

const Work = (props) => {
    return ( 
        <li>
            <h2>{props.title}</h2>
            <h3><strong>{props.jobTitle}</strong></h3>
            <h3>{props.dates}</h3>
            <p>{props.details}</p>
            <p><strong>Tools used:</strong> {props.tools}</p>
        </li>
    );
}

export default Work;

Work.propTypes = {
    title: PropTypes.string.isRequired,
    dates: PropTypes.string.isRequired,
    details: PropTypes.string.isRequired,
    jobTitle: PropTypes.string.isRequired,
    tools: PropTypes.string.isRequired
};