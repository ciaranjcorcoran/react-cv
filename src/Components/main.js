import React from 'react';
import Work from './Work/Work';
import SearchBar from './Work/SeachBar';
import PropTypes from 'prop-types';

const Main = (props) => {
  const filteredListTitles = props.jobList.map(job => {
    return <Work 
      title={job.title}
      jobTitle={job.job_title}
      dates={job.dates}
      details={job.details}
      tools={job.skills}
      key={job.key} />
  });

  return(
    <main>
      <SearchBar getJobs={props.searchJobs} />
      <ul>
        {filteredListTitles}
      </ul>
    </main>
  );
}

export default Main;

Main.propTypes = {
  searchJobs: PropTypes.func.isRequired
};